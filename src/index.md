---
layout: page
title: Textes
---

<ul>
  {% for post in collections.posts.resources %}
    <li>
      {{ post.data.date }} <a href="{{ post.relative_url }}">{{ post.data.title }}</a> {{ post.data.category | prepend: "#" }}
    </li>
  {% endfor %}
</ul>
