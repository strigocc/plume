---
layout: post
title: Le redouté
date: 2020-06-26
author: François Vantomme
category: poésie
description: Je viens de Téhéran…
image: images/2020/06/26/2021-04-24_penguicon_workshop_character-design_penguin-wizard.jpg
---

```
Je viens de Téhéran
Je vends du Thé, errant
De villes en vils
D'heure en heur
Ne redoutez
L'heure du thé
Tenez, goutez !
L'heureux doux thé.
```

{% rendercontent "figure", image: data.image %}
  “Pengalf” par David Revoy,
  [CC By](https://creativecommons.org/licenses/by/4.0/){:target="_blank"}{:rel="external"}
{% endrendercontent %}
