---
layout: post
title: Toujours en vadrouille
date: 2020-06-26
author: François Vantomme
category: poésie
description: Toujours en vadrouille
image: images/2020/06/26/2021-04-20_e35p01_color-panel-test.jpg
---

```
Toujours en vadrouille
Jamais ne se pose
Sans quoi l'esprit rouille
Faiblit, se sclérose.
```

{% rendercontent "figure", image: data.image %}
  “Flying Above the Clouds” par David Revoy,
  [CC By](https://creativecommons.org/licenses/by/4.0/){:target="_blank"}{:rel="external"}
{% endrendercontent %}
