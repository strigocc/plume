---
layout: post
date: 2023-07-18
title: Randonnée à Pied
category: pense-bête
description: Ce qu'il faut apporter avec soi pour une randonnée pédestre réussie.
image: /images/2023/07/18/toomas-tartes-Yizrl9N_eDA-unsplash.jpg
alimentation: site.data.checklists.alimentation
bagagerie: site.data.checklists.bagagerie
bivouac: site.data.checklists.bivouac
divers: site.data.checklists.divers
equipement: site.data.checklists.equipement
habillement: site.data.checklists.habillement
hygiene: site.data.checklists.hygiene
---

Cette liste est le fruit de nombreuses années de préparation de randonnées. Elle
a notamment été éprouvée sur le TMB, le GR20, et de nombreuses autres.

## Hygiène

### Mutualisable

{% render "tasklist", list: data.hygiene.mutualisable %}

> Astuce : la bassine peut être remplacée par un sac à vide d'air pour faire la
> lessive !

> MUL : un savon de Marseille peut servir tout à la fois de lessive, de savon et
> de shampooing !

### Individuel

{% render "tasklist", list: data.hygiene.individuel %}

> Astuce : préférez des dentifrice, savon, shampooing, déodorant solides pour
> éviter qu'ils ne se déversent dans votre sac ; mettez tout ce qui est liquide
> ou qui craint l'eau dans des sachets Ziplock !

## Habillement

{% assign habillement_individuel = data.habillement.individuel
                                 | concat: data.habillement.pied.individuel %}

{% render "tasklist", list: habillement_individuel %}

> Astuce : préférez les vêtements en laine Mérinos qui retient très peu les
> odeurs, et sèche très rapidement !

## Équipement

### Mutualisable

{% render "tasklist", list: data.equipement.mutualisable %}

> Astuce : un préservatif peut servir à étanchéifier un objet qui craint l'eau,
> transporter de l'eau, faire loupe pour allumer un feu… bref, c'est multi-usage
> et ça ne prend pas de place !

### Individuel

{% assign equipement_individuel = data.equipement.individuel
                                | concat: data.equipement.pied.individuel %}

{% render "tasklist", list: equipement_individuel %}

> Astuce : mettez vos vêtements dans des sacs compartiments ou des sacs à vide
> d'air pour optimiser le rangement de votre sac à dos et accéder facilement à
> vos affaires !

## Bivouac

### Mutualisable

{% render "tasklist", list: data.bivouac.mutualisable %}

> Astuce : un hamac peut remplacer une tente tout en étant plus léger ;
> attention cependant, il vous faudra trouver des arbres pour l'y accrocher !

> Astuce : une couverture de survie, face argentée vers le sol, protège
> efficacement de l'humidité qui ne manquera pas de remonter du sol dans la
> nuit !

> MUL : un tarp et une protection au sol peuvent suffire !

### Individuel

{% render "tasklist", list: data.bivouac.individuel %}

> MUL: votre sac de linge sale peut très bien faire office d'oreiller ;)

## Bagagerie

{% assign bagagerie_individuel = data.bagagerie.individuel
                               | concat: data.bagagerie.pied.individuel %}

{% render "tasklist", list: bagagerie_individuel %}

> Astuce : moins vous vous chargez, plus agréable sera le séjour ! Tâchez de ne
> pas dépasser les 12–15Kg pour une personne de 80Kg/1m80 et 8-10Kg pour une
> personne de 60kg/1m60. Un sac de 50-60L devrait être suffisant pour un séjour
> long de plusieurs jours/semaines.

## Alimentation

{% render "tasklist", list: data.alimentation.mutualisable %}

> Astuce : il n'est pas rare que les refuges proposent des paniers-repas pour le
> midi, n'hésitez pas à leur poser la question lors de votre arrivée !

### En autonomie

{% render "tasklist", list: data.alimentation.en-autonomie %}

## Divers

### Mutualisable

> **Important** : constituez-vous une [Trousse de secours][*].

[*]: {% post_url 2023-07-18-trousse-de-secours %}

{% render "tasklist", list: data.divers.mutualisable %}

> Astuce : un petit jeu de cartes ne prend pas de place et agrémente les soirées
> au refuge ; pensez à le conserver dans un Ziplock !

### Individuel

{% render "tasklist", list: data.divers.individuel %}

> Astuce : si vous partez à l'étranger, pensez à faire un peu de change avant
> votre départ !

*[GR20]: Fra li Monti, Corse (15 jours ; 179Km ; D+ 11000m)
*[MUL]: Marche Ultra-Légère
*[TMB]: Tour du Mont-Blanc (8 jours ; 16OKm ; D+ 9297m)
