---
layout: post
date: 2023-07-18
title: Randonnée à Vélo
category: pense-bête
description: Ce qu'il faut apporter avec soi pour une randonnée vélo réussie.
image: /images/2023/07/18/patrick-hendry-1ow9zrlldJU-unsplash.jpg
alimentation: site.data.checklists.alimentation
bagagerie: site.data.checklists.bagagerie
bivouac: site.data.checklists.bivouac
divers: site.data.checklists.divers
equipement: site.data.checklists.equipement
habillement: site.data.checklists.habillement
hygiene: site.data.checklists.hygiene
---

Cette liste est le fruit de plusieurs préparations de randonnées vélo. Elle a
notamment servi pour préparer Prague-Vienne et la Loire à Vélo.

## Hygiène

### Mutualisable

{% render "tasklist", list: data.hygiene.mutualisable %}

> Astuce : la bassine peut être remplacée par un sac à vide d'air pour faire la
> lessive !

> MUL : un savon de Marseille peut servir tout à la fois de lessive, de savon et
> de shampooing !

### Individuel

{% render "tasklist", list: data.hygiene.individuel %}

> Astuce : préférez des dentifrice, savon, shampooing, déodorant solides pour
> éviter qu'ils ne se déversent dans votre sac ; mettez tout ce qui est liquide
> ou qui craint l'eau dans des sachets Ziplock !

## Habillement

{% assign habillement_individuel = data.habillement.individuel
                                 | concat: data.habillement.velo.individuel %}

{% render "tasklist", list: habillement_individuel %}

> Astuce : préférez les vêtements en laine Mérinos qui retient très peu les
> odeurs, et sèche très rapidement !

## Équipement

### Mutualisable

{% assign equipement_mutualisable = data.equipement.mutualisable
                                  | concat: data.equipement.velo.mutualisable %}

{% render "tasklist", list: equipement_mutualisable %}

> Astuce : un préservatif peut servir à étanchéifier un objet qui craint l'eau,
> transporter de l'eau, faire loupe pour allumer un feu… bref, c'est multi-usage
> et ça ne prend pas de place !

> Astuce : les pattes de dérailleurs sont faites pour casser et sont spécifiques
> à chaque fabriquant, voire gamme ou modèle. Vous risquez de ne pas trouver la
> bonne si jamais elle cède !

### Individuel

{% assign equipement_individuel = data.equipement.individuel
                                | concat: data.equipement.velo.individuel %}

{% render "tasklist", list: equipement_individuel %}

> Astuce : mettez vos vêtements dans des sacs compartiments ou des sacs à vide
> d'air pour optimiser le rangement de vos sacoches et accéder facilement à
> vos affaires !

> MUL : certains éclairages vélo peuvent servir de frontale et de batterie !

## Bivouac

### Mutualisable

{% render "tasklist", list: data.bivouac.mutualisable %}

> Astuce : un hamac peut remplacer une tente tout en étant plus léger ;
> attention cependant, il vous faudra trouver des arbres pour l'y accrocher !

> Astuce : une couverture de survie, face argentée vers le sol, protège
> efficacement de l'humidité qui ne manquera pas de remonter du sol dans la
> nuit !

> MUL : un tarp et une protection au sol peuvent suffire !

### Individuel

{% render "tasklist", list: data.bivouac.individuel %}

> MUL: votre sac de linge sale peut très bien faire office d'oreiller ;)

## Bagagerie

{% assign bagagerie_individuel = data.bagagerie.individuel
                               | concat: data.bagagerie.velo.individuel %}

{% render "tasklist", list: bagagerie_individuel %}

> Astuce : chargez-vous au minimum et pensez à répartir le poids sur l'ensemble
> du vélo.

## Alimentation

{% render "tasklist", list: data.alimentation.mutualisable %}

> Astuce : il n'est pas rare que les campings proposent des paniers-repas pour le
> midi, n'hésitez pas à leur poser la question lors de votre arrivée !

### En autonomie

{% render "tasklist", list: data.alimentation.en-autonomie %}

## Divers

### Mutualisable

> **Important** : constituez-vous une [Trousse de secours][*].

[*]: {% post_url 2023-07-18-trousse-de-secours %}

{% render "tasklist", list: data.divers.mutualisable %}

> Astuce : un petit jeu de cartes ne prend pas de place et agrémente les
> soirées ; pensez à le conserver dans un Ziplock !

### Individuel

{% render "tasklist", list: data.divers.individuel %}

> Astuce : si vous partez à l'étranger, pensez à faire un peu de change avant
> votre départ !

*[Loire à Vélo]: Nantes-Orléans (5 jours ; 350Km ; D+ 1900m)
*[MUL]: Marche Ultra-Légère
*[Prague-Vienne]: 5 jours ; 350Km ; D+ 4000m
