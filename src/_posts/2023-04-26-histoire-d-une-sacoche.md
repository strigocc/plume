---
layout: post
title: Histoire d'une sacoche
date: 2023-04-26
author: François Vantomme
category: anecdote
description: Le weekend dernier j'ai emprunté le vélo électrique d'une amie…
image: images/2023/04/26/b007b90adf1b4e9f814a355a70c15d05.jpg
---

26 avril 2023, 20h12.

Le weekend dernier j'ai emprunté le vélo électrique d'une amie qui habite à côté
de là où se tient la permanence de Coagul. Son colloc, très sympa, m'a prêté ses
sacoches. Ce soir, je profite donc du trajet vers Coagul pour leur rapporter
vélo et sacoches. En arrivant chez eux, je m'aperçois qu'il manque une sacoche à
l'arrière du vélo. J'ai fait 7km. Je ne sais pas quand je l'ai perdue.
Évidemment, celle que j'ai perdue n'est pas vide ! Elle contient une chambre à
air, et le chargeur de la batterie du vélo ! Je reprends donc la route en sens
inverse. Les yeux grands ouverts à la recherche d'une sacoche jaune fluo
immanquable. Et j'arrive chez moi. 7km. Pas de sacoche. Je monte boire un verre
d'eau, je peste, redescends, enfourche mon vélo, et refait le trajet une
troisième fois. Arrivé au tram, à 1km de chez moi, je trouve une pancarte fixée
à un poteau. Je l'avais aperçue à l'aller, mais ne m'y étais pas arrêté. Elle
disait…

{% rendercontent "figure", image: data.image %}
  J'AI TROVER ICI UN SAC AVEC CHARGEUR DE VELO / SI PROPRIETAIRE APPELER
  ##########<br/>
  il faut savoir la marque ; couleur du sac et quoi d'autre a
  a l'interieur du SAC<br/>
  “Annonce en carton” par François Vantomme,
  [CC0](https://creativecommons.org/publicdomain/zero/1.0/){:target="_blank"}{:rel="external"}
{% endrendercontent %}

Je fouille mes poches à la recherche de mon portable. Je ne l'ai pas. Je l'ai
laissé dans mon sac à dos, chez les colocs. J'interpelle alors un couple passant
par là. Je leur demande s'ils peuvent composer ce numéro pour que je puisse
récupérer ma sacoche. La nana sort son téléphone, compose sous mes yeux le
numéro indiqué sur la pancarte, et active le haut-parleur. Un homme décroche, et
un interrogatoire musclé commence. Je lui détaille le contenu de la sacoche, sa
couleur, sa marque, la taille de la chambre à air (29"). Ce n'est pas suffisant.
Il me faut lui donner la marque du chargeur ! Eh bien, euh… Gazelle, le vélo
c'est un Gazelle. Non. Shimano, comme le moteur ? Oui. C'est ça ! Il me dit
ensuite que je peux le retrouver rue Sadi Carnot, qu'il est en foyer, et qu'il
n'a plus le droit de sortir à cette heure. Il est 21h30. Le conjoint de la femme
qui me tend toujours son portable, sort le sien et cherche ladite rue sur une
carte. C'est à 5 minutes à vélo. Sur ce, le détenteur de la sacoche me demande
comment ça va dans ma vie ? Si je suis à l'aise financièrement ? Parce que lui,
voyez-vous, il est dans la galère, à la rue, alors il ne serait pas contre un
petit billet. Je lui promets d'acheter de l'argent si je croise une banque. Je
remercie le couple très charmant qui m'a dépanné, et leur fait savoir que
l'anecdote, c'est cadeau ! J'enfourche de nouveau le vélo, pédale jusqu'à la rue
Carnot, trouve le foyer, et sonne. Puis sonne à nouveau. Rien. De la lumière,
mais personne. Je sonne une troisième fois, là un éducateur vient m'ouvrir,
suivi de l'opportuniste débrouillard qui, il faut le rappeler, a trouvé un
carton et un feutre pour y laisser un message à mon attention ! Il me tend la
sacoche, et m'invite à en vérifier le contenu. Rien ne manque. Soulagé, et
respectant ma promesse, je lui tends un billet pour le dédommagement ! Les
salutations faites, me voilà de nouveau en route. Plus loin de mon arrivée que
je ne l'étais à mon point de départ. Et 25 minutes plus tard, les effets sont
restitués à leur propriétaire respectif, qui me proposent une tisane en échange
de cette anecdote :) Fin.

## Épilogue

J'ai poussé à pied jusqu'à Coagul, mais j'ai trouvé porte close, tout le monde
était déjà reparti !


*[Coagul]: Côte-d'Or - Association Générale des Utilisateurs de Logiciels libres
