---
layout: post
date: 2023-07-18
title: Trousse de secours
category: pense-bête
description: Comment constituer sa trousse de secours ?
image: /images/2023/07/18/mat-napo-MaKsx8JNbiI-unsplash.jpg
---

> Avertissement : Les informations contenues dans ce pense-bête sont fournies à titre indicatif seulement et ne constituent en aucun cas un avis médical ni une recommandation quelconque. Elles ne doivent pas être utilisées pour fonder une quelconque décision d'apporter un soin à autrui ou soi-même. **DANS TOUS LES CAS** : consulter un médecin en cas d'urgence ou de doute.

## Petit matériel

- [ ] Gel nettoyant pour les mains – 2 ou 3 paires de gants en latex
- [ ] Ciseaux – Pince à écharde – Épingles à nourrice
- [ ] Compresses stériles – Coton
- [ ] Sparadrap prédécoupé – Collant type URGOPORE
- [ ] Bandes nylon et bandes Velpo
- [ ] Thermomètre
- [ ] Couverture de survie
- [ ] Aspivenin
- [ ] Tire-tique
- [ ] Purificateur d’eau — MICROPUR Forte
- [ ] Pansements
- [ ] Pansements à ampoules

## Plaies

- [ ] Nettoyage des plaies : Sérum Physiologique (dosettes)
- [ ] Désinfection plaies : BISEPTINE (spray ou dosettes)
- [ ] Séchage des plaies : Eosine (dosettes)
- [ ] "Suture" des plaies ouvertes : STERISTRIP
- [ ] Plaies sales, brûlures : Tulle gras
- [ ] Petites plaies infectées : pommade FUCIDINE / MUPIDERM / MUPIROCINE

## Douleurs et/ou fièvre

- [ ] Paracétamol 500mg — DOLIPRANE / EFFERALGAN / DAFALGAN / GELUPRANE / DOLKO / IBUPROFENE / KETOPROFÈNE

## Saignements de nez

- [ ] COALGAN

## Entorses

- [ ] NIFLUGEL / NIFLURIL pommade / VOLTARENE / SRILANE / ADVIL Gel / ARNIGEL

## Douleurs musculaires

- [ ] Bande auto-agrippante / ELASTOPLASTE
- [ ] Chevillère, Genouillère

## Contusions, hématomes

- [ ] Pommade à l'Arnica

## Coups de soleil, brûlures cutanées légères

- [ ] BIAFINE

## Piqûres d'insectes

- [ ] Pommade DIPROSONE / NERISONE / LOCAPRED / LOCOID / APTONIA / CORTAPAISYL

## Démangeaisons

- [ ] Hydrocortisone

## Diarrhée

- [ ] Sachets SMECTA / ACTAPULGITE / BEDELIX / PÉRACEL / Lopéramide / Ultra Levure

## Constipation

- [ ] FORLAX / MACROGOL

## Douleurs intestinales

- [ ] SPASFON / DOMPÉRIDONE

## Boutons de fièvre

- [ ] Crème ACICLOVIR / ZOVIRAX / CICALFATE

## Aphtes

- [ ] Liso6 / HYALUGEL

## Lèvres gercées

- [ ] Stick lèvres — LABELLO

## Hydratation visage et mains

- [ ] Crème hydratante — DEXERYL / NIVEA / Avène Cicalfate

## Allergies

- [ ] XYZALL / Cétirizine / Lévocétirizine / Desloratadine (Aerius) / Bilastine

---

> **IMPORTANT !**
> Ne pas oublier votre carte VITALE et traitements en cours s'il y a lieu.

> **DANS TOUS LES CAS** : consulter un médecin en cas d'urgence ou de doute.
