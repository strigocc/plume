---
layout: post
title: À l'orée d'Auray
date: 2020-06-26
author: François Vantomme
category: poésie
description: À l'orée d'Auray, Laure est dorée…
image: images/2020/06/26/2022-03-02_In-Bloom_by-David-Revoy.jpg
---

```
À l'orée d'Auray, Laure est dorée
L'eau, raies dorées, dort, au lit
L'eau d'or reluit, Et Laure dore et luit
Dès lors, Laure est l’eau, et l'or les lie
Alors Laure dort et lit et dore à la lie
Héla Élie, et lui alla. Élie est là.
Et Laure et lui, d'Auray fuient.
```

{% rendercontent "figure", image: data.image %}
  “In Bloom” par David Revoy,
  [CC By](https://creativecommons.org/licenses/by/4.0/){:target="_blank"}{:rel="external"}
{% endrendercontent %}
