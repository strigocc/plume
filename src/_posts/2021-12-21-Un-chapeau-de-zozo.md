---
layout: post
title: Un chapeau de zozo
date: 2021-12-21
author: François Vantomme
category: poésie
description: Un chapeau de zozo…
image: images/2021/12/21/2006-chapeau-champi_014_by-David-Revoy.jpg
---

```
Un chapeau de zozo
S'échappa de Zaza
Zaza qu'était z'au zoo
Que dis-je ! qu'était aux eaux

Aux eaux, elle s'y jeta
Sans chape, oh ! elle osa
D'un élan elle choppa
Son chapeau de zozo.
```

{% rendercontent "figure", image: data.image %}
  “Mushroom hat” par David Revoy,
  [CC By-NC-ND](https://creativecommons.org/licenses/by-nc-nd/4.0/){:target="_blank"}{:rel="external"}
{% endrendercontent %}
