---
layout: post
title: Un absolu bonheur
date: 2023-04-14
author: François Vantomme
category: poésie
description: Un absolu bonheur, me voilà bien en veine…
image: images/2023/04/14/2010-07-07-mermaid-finalb_by_david-revoy.jpg
---

```
Un absolu bonheur, me voilà bien en veine
Ne puis-je donc par moment épouver quelque peine ?
Varier les sentiments, apporter profondeur
Sans tomber dans l'abus, la souffrance, le malheur
Souhaitez-moi plutôt pour garnir ces jours fastes
Une palette d'impressions, pour tout dire du contraste !
```

{% rendercontent "figure", image: data.image %}
  “Fractal mermaid” par David Revoy,
  [CC By](https://creativecommons.org/licenses/by/4.0/){:target="_blank"}{:rel="external"}
{% endrendercontent %}
