---
layout: post
date: 2023-07-18
title: Huiles essentielles
category: pense-bête
description: Quelles huiles essentielles aporter en camping ou en rando ?
image: /images/2023/07/18/chelsea-gates-LmMVBaZyHoI-unsplash.jpg
---

> Avertissement : Les informations contenues dans ce pense-bête sont fournies à titre indicatif seulement et ne constituent en aucun cas un avis médical ni une recommandation quelconque. Elles ne doivent pas être utilisées pour fonder une quelconque décision d'apporter un soin à autrui ou soi-même. **DANS TOUS LES CAS** : consulter un médecin en cas d'urgence ou de doute.

## Ylang-Ylang

>  Diluer 1 goutte dans 4 gouttes d'huile végétale

- Douleurs articulaires
- Douleurs dentaires
- Douleurs musculaires, crampes, contractures
- Douleurs spasmodiques
- Fièvre
- Problèmes de circulation sanguine
- Règles douloureuses
- Stimulante intellectuelle
- Stimulante sexuelle
- Troubles ORL et respiratoires
- Troubles digestifs

## Menthe Poivrée

>  Diluer très fortement avec une huile végétale (pas plus de 3%)

- Choc, coup immédiat (sans plaie ouverte)
- Cicatrices
- Diarrhée aigüe
- Infections ORL
- Jambes gonflées ou lourdes
- Mal des transports
- Migraine et céphalée
- Nausées ou vomissements
- Nez bouché
- Piqûre d’insecte
- Plaies
- Problèmes digestifs
- Règles douloureuses
- Troubles ORL et respiratoires
- Troubles digestifs (ballonnements, aérophagie, indigestion…)

## Lavande Aspic

>  1 à 5 gouttes pures sur les zones concernées

- Boutons d’acné
- Brûlure (sans plaie ouverte)
- Coup de soleil
- Dermatose (herpès labial, eczéma, zona, varicelle, prurit, mycose cutanée, psoriasis, couperose …)
- Douleurs musculaires, crampes, contractures
- Escarre
- Fatigue physique et psychique
- Migraine et céphalée
- Piqûre d’insecte et de plante
- Problèmes cutanés
- Spasmes abdominaux nerveux (adultes)
- Troubles ORL et respiratoires
- Éloigner les insectes

## Gaulthérie Couchée

>  Diluer 1 goutte dans 4 gouttes d’une huile végétale

- Arthrite inflammatoire, polyarthrite, arthrose vertébrale
- Crampe
- Douleurs articulaires
- Douleurs musculaires, contractures, courbatures
- Effort sportif (en préparation et récupération)
- Problèmes de circulation
- Rhumatismes
- Tendinite

## Eucalyptus Radiata

>  1 goutte dans 4 gouttes d'une huile végétale

- Fatigue
- Infections respiratoires et ORL (bronchite, grippe, rhinopharyngite, sinusite)
- Toux grasse (jamais en cas de toux sèche)
- Troubles ORL et respiratoires
- Troubles immunitaires (fatigue, infections virales)

## Arbre à Thé

>  Pure ou diluée

- Fatigue
- Infections et troubles cutanés (furoncle, impétigo, mycose, zona, varicelle, psoriasis, panaris)
- Infections intestinales
- Infections respiratoires et ORL (bronchite, grippe, rhinopharyngite, sinusite)
- Infections uro-génitales (cystite, mycose vaginale, herpès génital, prurit, vulvo-vaginite)
- Inflammation des gencives
- Mal de gorge
- Pieds (fatigués, meurtris, etc.)
- Piqûre d’insecte
- Problèmes bucco-dentaires
- Problèmes de peaux (herpès labial, ulcère buccal, aphte, acné, eczéma suintant, démangeaisons)
- Renforcement du système immunitaire
- Stress
- Troubles ORL

---

> **DANS TOUS LES CAS** : consulter un médecin en cas d'urgence ou de doute.

*[ORL]: Otorhinolaryngologique
