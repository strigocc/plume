---
layout: post
title: Le vague a bon
date: 2020-06-26
author: François Vantomme
category: poésie
description: Contre toute attente…
image: images/2020/06/26/2018-04-25_lanternfish_random-speedpainting-streaming_by-David-Revoy.jpg
---

```
Contre toute attente,
Il est plus aisé
De vagabonder au pif
Que de voyager à l'œil !
```

{% rendercontent "figure", image: data.image %}
  “Lantern Fish” par David Revoy,
  [CC By](https://creativecommons.org/licenses/by/4.0/){:target="_blank"}{:rel="external"}
{% endrendercontent %}
