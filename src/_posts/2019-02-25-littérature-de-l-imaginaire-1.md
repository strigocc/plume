---
layout: post
title: Littérature de l'imaginaire ↬ S.F.
date: 2019-02-25
author: François Vantomme
category: bavardages
description: Quelques-unes des œuvres de Science-Fiction qui ont frappé mon parcours de lecteur.
canonical: https://write.as/akarzim/litterature-de-limaginaire-1-2
image: /images/2019/02/25/mission-by-David-Revoy.jpg
---

J'ai envie de parler de littérature de l'imaginaire, de partager quelques-unes
des œuvres Fantasy et SF qui ont frappé mon parcours de lecteur. Je vais tenter
de me limiter aux œuvres et auteurs les plus marquants, en parcourant le large
spectre que recouvre ce genre littéraire.

{% rendercontent "figure", image: data.image %}
  “Mission” par David Revoy,
  [CC By](https://creativecommons.org/licenses/by/4.0/){:target="_blank"}{:rel="external"}
{% endrendercontent %}

En Science-Fiction tout d'abord, impossible de passer à côté d'[Isaac Asimov][+]
(1920-1992), auteur prolifique qui a donné ses lettres de noblesse au genre,
inventeur du terme robotique — mais pas du mot robot que l'on découvre pour la
première fois dans la pièce de théâtre [R.U.R.][+] du tchèque [Karel Čapek][+]
(1890–1938) — et des trois lois de la robotique qui serviront de pilier à son
œuvre et qu'il éprouvera de mille et une façons pour en révéler les failles et
les limites. C'est ce qui constitue la principale substance du [Cycle des
Robots][+] qui se présente principalement sous forme de recueils de nouvelles.
Mais on lui connait surtout le [Cycle de Fondation][+] qui se présente comme une
histoire du futur, 22 000 ans après notre ère, où une nouvelle science nommée
psycho-histoire promet à l'humanité qui a essaimé la galaxie de lui épargner
30 000 ans de barbarie au prix de mille ans de chaos. Certains de ses livres ont
fait l'objet d'une adaptation en film tel que [I, Robot][+] ou encore [L'Homme
Bicentenaire][+].

[+]: https://fr.wikipedia.org/wiki/Isaac_Asimov
[+]: https://fr.wikipedia.org/wiki/R._U._R.
[+]: https://fr.wikipedia.org/wiki/Karel_Čapek
[+]: https://fr.wikipedia.org/wiki/Cycle_des_robots
[+]: https://fr.wikipedia.org/wiki/Cycle_de_Fondation
[+]: https://fr.wikipedia.org/wiki/I,_Robot_(film)
[+]: https://fr.wikipedia.org/wiki/L'Homme_bicentenaire_(film)

En Science-Fiction toujours, comment ne pas citer [Frank Herbert][+]
(1920-1986), auteur du [Cycle de Dune][+], monument de la SF qui aborde de
nombreux thèmes comme l'écologie, la survie de l'espèce humaine, la
géopolitique, la religion… incontournable !

[+]: https://fr.wikipedia.org/wiki/Frank_Herbert
[+]: https://fr.wikipedia.org/wiki/Dune_(franchise)

Puis, parmi les auteurs plus méconnus, en France notamment, qui ont marqué
l'histoire de la SF, il faut nommer [Cordwainer Smith][+] (1913-1966) et son
cycle [Les Seigneurs de l'Immortalité][+] qui nous propulse 14 000 ans dans le
futur où l'humanité est maintenue dans un état d'utopie stérile sous le joug
d'une dictature bienveillante, l'occasion pour l'auteur d'aborder des thèmes
comme la psychologie, le racisme et la religion.

[+]: https://fr.wikipedia.org/wiki/Cordwainer_Smith
[+]: https://fr.wikipedia.org/wiki/Les_Seigneurs_de_l'instrumentalité

Au rang des classiques je retiens [H.G. Wells][+] (1866-1946) : [La Guerre des
Mondes][+], [L'Homme Invisible][+], [La Machine à Explorer le Temps][+] ;
[George Orwell][+] (1903-1950) avec notamment [1984][+] ; plus récemment [Orson
Scott Card][+] (1951-) et son [Cycle d'Ender][+] ; et un de mes petits chouchous
[Des Fleurs pour Algernon][+] de [Daniel Keyes][+] (1927-2014) !

[+]: https://fr.wikipedia.org/wiki/H._G._Wells
[+]: https://fr.wikipedia.org/wiki/La_Guerre_des_mondes
[+]: https://fr.wikipedia.org/wiki/L'Homme_invisible_(roman)
[+]: https://fr.wikipedia.org/wiki/La_Machine_à_explorer_le_temps
[+]: https://fr.wikipedia.org/wiki/George_Orwell
[+]: https://fr.wikipedia.org/wiki/1984_(roman)
[+]: https://fr.wikipedia.org/wiki/Orson_Scott_Card
[+]: https://fr.wikipedia.org/wiki/Cycle_d'Ender
[+]: https://fr.wikipedia.org/wiki/Des_fleurs_pour_Algernon
[+]: https://fr.wikipedia.org/wiki/Daniel_Keyes

Parmi les inclassables, il faut citer [Alain Damasio][+] (1969-), auteur
français, techno-sceptique militant et engagé, jonglant avec les mots comme avec
des couteaux affutés, à qui l'on doit [La Zone du Dehors][+] et, à mi-chemin
entre Science-Fiction et Fantasy, [La Horde du Contrevent][+], un pur
chef-d'œuvre !

[+]: https://fr.wikipedia.org/wiki/Alain_Damasio
[+]: https://fr.wikipedia.org/wiki/La_Zone_du_Dehors
[+]: http://www.elbakin.net/fantasy/roman/la-horde-du-contrevent-919

Enfin, pour faire la transition vers la Fantasy, qui de mieux qu'[Ursula K. Le
Guin][+] (1929-2018) dont les œuvres de Science-Fiction de son [Cycle de
l'Ekumen][+] délaissent quelque peu la technologie pour font la part belle aux
sciences sociales. Mais elle est aussi reconnue en Fantasy pour son [Cycle de
Terremer][+], qui a d'ailleurs donné lieu à une [adaptation par les studios
Ghibli][+] !

[+]: https://fr.wikipedia.org/wiki/Ursula_K._Le_Guin
[+]: https://fr.wikipedia.org/wiki/Cycle_de_l'Ekumen
[+]: https://fr.wikipedia.org/wiki/Cycle_de_Terremer
[+]: https://fr.wikipedia.org/wiki/Les_Contes_de_Terremer

Oulà ! Je ne pensais pas écrire autant et nous n'avons fait qu'effleurer la SF !
Je me fendrai d'un second billet pour parler de Fantasy ;)

Bonne lecture !
