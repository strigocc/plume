---
layout: post
title: Littérature de l'imaginaire ↬ Fantasy
date: 2019-03-03
author: François Vantomme
category: bavardages
description: Quelques-unes des œuvres de Fantasy qui ont frappé mon parcours de lecteur.
canonical: https://write.as/akarzim/litterature-de-limaginaire-2-2
image: /images/2019/03/03/village-in-the-air-by-David-Revoy.jpg
---

À la fin de la [première partie][*] de ce diptyque sur la littérature de
l'imaginaire, nous nous étions quittés avec [Ursula K. Le Guin][+] (1929–2018) et son
cycle de [Terremer][+], faisant ainsi une parfaite transition de la Science-Fiction
vers la Fantasy.

[*]: {% post_url 2019-02-25-littérature-de-l-imaginaire-1 %}
[+]: https://fr.wikipedia.org/wiki/Ursula_K._Le_Guin
[+]: https://fr.wikipedia.org/wiki/Cycle_de_Terremer

S'il est un genre difficile à définir, c'est bien celui de la Fantasy, tant il
présente de visages et se réinvente sans cesse ! Je vous propose donc un aperçu
de cette diversité par le prisme des auteurs qui ont marqué mon parcours de
lecteur.

{% rendercontent "figure", image: data.image %}
  “Village in the air” par David Revoy,
  [CC By-NC-ND](https://creativecommons.org/licenses/by-nc-nd/4.0/){:target="_blank"}{:rel="external"}
{% endrendercontent %}

J'ai découvert la Fantasy, comme beaucoup de lecteurs, grâce à [J.R.R.
Tolkien][+] (1892–1973) et son [Seigneur des Anneaux][+], que je ne vous ferai
pas l'affront de présenter. Après la lecture du [Hobbit][+], au demeurant bien
plus accessible, j'ai longtemps hésité à me replonger dans la Fantasy, persuadé
que la Fantasy c'était Tolkien, ses elfes et ses hobbits, et que tout autre
ouvrage ne serait que pâle copie.

[+]: https://fr.wikipedia.org/wiki/J._R._R._Tolkien
[+]: http://www.elbakin.net/fantasy/roman/le-seigneur-des-anneaux-1755
[+]: http://www.elbakin.net/fantasy/roman/le-hobbit-1756

C'était sans compter sur [Terry Pratchett][+] (1948–2015) qui, avec ses [Annales
du Disque-Monde][+], un cycle de 41 romans burlesques à l'humour anglais
pénétrant, interroge notre société par le truchement d'un univers riche et
fouillé où cohabitent tant bien que mal mages, sorcières, nains, trolls, et même
la Mort personnifiée. Et ce ne sont pas les sujets qui manquent : la Fantasy
bien sûr, mais aussi la presse, l'apocalypse, l'industrie du cinéma, le théâtre,
Noël, les religions, le rock'n'roll… tout y passe.

[+]: https://fr.wikipedia.org/wiki/Terry_Pratchett
[+]: http://www.elbakin.net/fantasy/roman/cycle/les-annales-du-disque-monde-14

Terry Pratchett a également collaboré avec d'autres auteurs de talent. Et la
transition est toute trouvée pour introduire [Neil Gaiman][+] (1960–), dont la
collaboration avec Pratchett a donné naissance à [De Bons Présages][+], un roman
mêlant avec brio l'humour de Pratchett et les talents de conteur de Gaiman !

[+]: https://fr.wikipedia.org/wiki/Neil_Gaiman
[+]: http://www.elbakin.net/fantasy/roman/de-bons-presages-544

Neil Gaiman est sans pareil ! Il nous entraîne dans son univers dès les trois
premières lignes. Avec ce style remarquable qui est le sien, il nous emmène à la
lisière du réel et du fantastique, du tangible et de l'onirique, pour finalement
nous rendre à la réalité, un peu transformé. Parmi ses œuvres principales, on
peut citer [L'étrange vie de Nobody Owens][+], [Neverwhere][+], [American
Gods][+], [Stardust][+] ou encore [Coraline][+].

[+]: http://www.elbakin.net/fantasy/roman/l-etrange-vie-de-nobody-owens-226
[+]: http://www.elbakin.net/fantasy/roman/neverwhere-987
[+]: http://www.elbakin.net/fantasy/roman/american-gods-405
[+]: http://www.elbakin.net/fantasy/roman/stardust-le-mystere-de-l-etoile-1289
[+]: http://www.elbakin.net/fantasy/roman/coraline-794

Ce qui caractérise la Fantasy, ce sont ses ambiances, ses univers ! Il est des
auteurs qui, à l'instar de Tolkien et sa Terre du Milieu, créent des univers
complets au sein desquels évoluent leurs personnages et se déroulent leurs
intrigues. [Brandon Sanderson][+] (1975–) fait partie de ceux-là. Son univers
Cosmère est la scène de théâtre de ses plus grands cycles tels que
[Fils-des-brumes][+], [Les Archives de Roshar][+], ou encore [Elantris][+]. Ce
dernier est d'ailleurs une excellente porte d'entrée pour découvrir Sanderson.

[+]: https://fr.wikipedia.org/wiki/Brandon_Sanderson
[+]: http://www.elbakin.net/fantasy/roman/cycle/fils-des-brumes-748
[+]: http://www.elbakin.net/fantasy/roman/cycle/les-archives-de-roshar-1211
[+]: http://www.elbakin.net/fantasy/roman/elantris-847

Un autre grand bâtisseur d'univers, auteur de High-Fantasy à qui l'on doit [La
Roue du Temps][+], une œuvre en 14 volumes, c'est [Robert Jordan][+] (1948–2007). À son
décès, sa veuve demanda à Brandon Sanderson de rédiger la fin de ce cycle qui
s'impose comme un monument de la Fantasy.

[+]: http://www.elbakin.net/fantasy/roman/cycle/la-roue-du-temps-226
[+]: https://en.wikipedia.org/wiki/Robert_Jordan

Et quand la construction d'univers rencontre la Fantasy épique, c'est le nom de
[Steven Erikson][+] (1959–) qui vient à l'esprit. Il est l'auteur d'une œuvre
magistrale, [Le livre des Martyrs][+], une décalogie d'une ampleur inégalée, où
rien n'est laissé au hasard, tant au niveau du rythme, que du style ou encore de
l'originalité et de l'inventivité dont fait preuve l'auteur, notamment en ce qui
concerne les systèmes de magie déployés.

[+]: https://fr.wikipedia.org/wiki/Steven_Erikson?oldformat=true
[+]: http://www.elbakin.net/fantasy/roman/cycle/le-livre-des-martyrs-77

Mais on ne saurait parler de Fantasy sans mentionner l'Heroic-Fantasy dont l'un
des fondateurs du genre n'est autre que [Robert E. Howard][+] (1906–1936), le père de
[Conan le Cimmérien][+], aussi connu sous le nom de Conan le Barbare. Loin du cliché
du barbare analphabète à la musculature inversement proportionnelle à la taille
de son minuscule pagne en peau de bête, l'œuvre d'Howard se veut profonde et
pose un regard critique sur la civilisation telle que nous la connaissons.

[+]: https://fr.wikipedia.org/wiki/Robert_E._Howard
[+]: http://www.elbakin.net/fantasy/roman/cycle/conan-l-integrale-713

Alors que Robert E. Howard place les aventures de Conan dans un passé phantasmé
emprunt de mythologie, entre la chute de l'Atlantide et l'essor des premières
civilisations de notre âge, d'autres font de l'élément historique le terreau de
leur imaginaire. C'est le cas de [Guy Gavriel Kay][+] (1954–) qui s'élève au
rang de maître dans l'art de la Fantasy historique. Il est l'auteur de plusieurs
cycles comme [La Tapisserie de Fionavar][+] emprunt de mythologies celte et
nordique, mais aussi influencé par la légende arthurienne, [La Mosaïque de
Sarance][+] qui nous plonge dans l'empire byzantin du VIᵉ siècle, ou encore [Les
Chevaux Célestes][+] qui nous emmène en Empire du Milieu, la Chine de la
dynastie Tang.

[+]: https://fr.wikipedia.org/wiki/Guy_Gavriel_Kay
[+]: http://www.elbakin.net/fantasy/roman/cycle/la-tapisserie-de-fionavar-284
[+]: http://www.elbakin.net/fantasy/roman/cycle/la-mosaique-de-sarance-465
[+]: http://www.elbakin.net/fantasy/roman/les-chevaux-celestes-4049

C'est aussi la Chine que choisit [Estelle Faye][+] (1978–) pour planter le décor de
[Porcelaine, légende du tigre et de la tisseuse][+] un roman tout en légèreté et en
poésie. Estelle Faye c'est aussi [La Voie des Oracles][+], une trilogie dans le décor
de la Gaule du Vᵉ siècle, estampillée jeunesse, mais ne vous y trompez pas, il
ne s'agit là que d'une étiquette.

[+]: https://fr.wikipedia.org/wiki/Estelle_Faye
[+]: http://www.elbakin.net/fantasy/roman/porcelaine-3963
[+]: http://www.elbakin.net/fantasy/roman/cycle/la-voie-des-oracles-1273

Voyageons encore un peu et faisons place à la Science-Fantasy. Direction
l'Afrique avec [NNedi Okorafor][+] (1974–), autrice américaine d'origine nigérienne
qui, avec son roman [Qui a peur de la mort ?][+] nous plonge dans une Afrique
subsaharienne post-apocalyptique, mêlant technologie, tradition et sorcellerie.

[+]: https://fr.wikipedia.org/wiki/Nnedi_Okorafor
[+]: http://www.elbakin.net/fantasy/roman/qui-a-peur-de-la-mort-4228

Et puisque que nous en sommes aux autrices de Fantasy qui m'ont marqué, je me
dois ici de nommer [Jacqueline Carey][+] (1964–). Sa série [Kushiel][+] constituée de
trois trilogies, est sublime ! Dans un univers imprégné de Renaissance
flamboyante, Jacqueline Carey nous relate l'existence d'une jeune fille au
destin extraordinaire sur fond d'intrigues de palais et d'érotisme.

[+]: https://fr.wikipedia.org/wiki/Jacqueline_Carey
[+]: http://www.elbakin.net/fantasy/roman/cycle/kushiel-474

Restons dans la flamboyance et les complots avec [Jean-Philippe Jaworski][+]
(1969–) à qui l'on doit en particulier [Gagner la guerre][+] et [Janua Vera][+],
respectivement roman et recueil de nouvelles, inscrits dans l'univers du Vieux
Royaume. Ici aussi, avec Jaworski, sitôt les premières pages avalées, il est
difficile d'en décrocher tant les personnages sont charismatiques et l'univers
soigné ! Mais ce qui frappe avant tout chez Jaworski, c'est son style. Ce style
qui va donner du corps et de la consistance au récit, qui vous saisit et vous
bringuebale sans ménagement, sans manières, un peu rustre parfois, mais toujours
avec le mot juste ! Un vrai régal.

[+]: https://fr.wikipedia.org/wiki/Jean-Philippe_Jaworski?oldformat=true
[+]: http://www.elbakin.net/fantasy/roman/gagner-la-guerre-1356
[+]: http://www.elbakin.net/fantasy/roman/janua-vera-1355

Et si vous aimez la gouaille et le panache, c'est vers [Scott Lynch][+] (1978–)
et ses [Salauds Gentilshommes][+] qu'il faudra vous tourner ! Voilà un auteur
qui se montre implacable avec ses personnages qu'il nous invite à suivre dans
des situations indécrottables, desquelles ils tenteront de se dépêtrer en
mettant sur pied des arnaques renversantes avec un culot inouï.

[+]: https://fr.wikipedia.org/wiki/Scott_Lynch
[+]: http://www.elbakin.net/fantasy/roman/cycle/les-salauds-gentilshommes-334

Dans un genre très différent, entre mythes et mystère, [La Forêt des
Mythagos][+] de [Robert Holdstock][+] (1948–2009) est une œuvre tout à fait
remarquable ! Une véritable petite pépite dans un écrin de songe mystique.

[+]: http://www.elbakin.net/fantasy/roman/cycle/la-foret-des-mythagos-246
[+]: https://en.wikipedia.org/wiki/Robert_Holdstock

Enfin, n'oublions pas les ouvrages qui, sans devenir des classiques, ont fait de
la Fantasy ce qu'elle est aujourd'hui : un genre riche et en perpétuelle
redécouverte. Je pense évidemment au [Serpent Ouroboros][+] de [E.R. Eddison][+]
(1882–1945), [Lud-en-Brumes][+] de [Hope Mirrlees][+] (1887–1978), [Le Loup des
steppes][+] de [Harold Lamb][+] (1892–1962), pour ne citer que ceux-là.

[+]: https://www.editions-callidor.com/infos-2/jarduof530/Le-Serpent-Ouroboros-volume-1
[+]: https://fr.wikipedia.org/wiki/E._R._Eddison
[+]: https://www.editions-callidor.com/infos-2/NewsPostsItem0_i7cb6e8v90_0/LudenBrume
[+]: https://fr.wikipedia.org/wiki/Hope_Mirrlees
[+]: https://www.editions-callidor.com/infos-2/NewsPostsItem1_i7cb6e8v91_1/Le-Loup-des-steppes
[+]: https://en.wikipedia.org/wiki/Harold_Lamb

Voilà, j'espère que ce petit tour vous aura donné envie de lire de la Fantasy et
de la Science-Fiction, de découvrir ou redécouvrir des auteurs, des univers, des
ambiances et des styles !

Bonne lecture.
