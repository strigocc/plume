---
layout: post
title: Eiffel, cette grande tour
date: 2020-06-26
author: François Vantomme
category: poésie
description: Eiffel, cette grande tour…
image: images/2020/06/26/2006-Hell-on-earth-hi_by-David-Revoy.jpg
---

```
Eiffel, cette grande tour
Offerte à dame Nation
En fer, et contre toute
  [attente
Défie avec passion,
Patience et déraison,
Envers et contre tout
Enfer et damnation !
```

{% rendercontent "figure", image: data.image %}
  “Hell on earth” par David Revoy,
  [CC By-NC-ND](https://creativecommons.org/licenses/by-nc-nd/4.0/){:target="_blank"}{:rel="external"}
{% endrendercontent %}
