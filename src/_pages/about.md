---
layout: page
title: À propos
template_engine: erb
---

<%= site.metadata.title %> relate les dérives littéraires d'un passionné de
littérature de l'imaginaire, fondu de typographie, biberoné aux calembours de
Monsieur Bonheur et du [Grand Défenseur de la Poterie][+], dévôt du [Grand
Raymond][+], touché en plein cœur par la sainte flèche du [Grand Bobby][+].

[+]: https://fr.wikipedia.org/wiki/François_Pérusse
[+]: https://fr.wikipedia.org/wiki/Raymond_Devos
[+]: https://fr.wikipedia.org/wiki/Boby_Lapointe

<% site.taxonomy_types.category.terms.keys.each_with_index do |category, idx| %>
  <%= "· " unless idx.zero? %><a href="<%= relative_url('/categories/' + category) %>">#<%= category %></a>
<% end %>

---

Ce site a fait l'objet du plus grand soin pour le rendre [accessible au plus
grand nombre][+] et [réduire son empreinte environnementale][+]. Sa légèreté et
son minimalisme en font un site performant et lisible sur tout support. Les
textes de ce site sont également proposés sous forme de [flux RSS][+].

[+]: https://accessibilite.numerique.gouv.fr/
[+]: https://yellowlab.tools/result/gmiqwk0blv
[+]: https://plume.strigo.cc/feed.xml

La réalisation de ce site a été rendue possible grâce à [Bridgetown][+], un
généréateur de site statique en Ruby, [Liquid][+], [TailwindCSS][+], le thème de
couleurs [Nord][+], et la police de caractères [Yrsa][+]. Le logo provient du
jeu d'icônes [Feather][+]. Le tout déployé via [GitLab Pages][+]. L'ensemble des
sources de ce site est disponible dans un [dépôt GitLab][+].

[+]: https://www.bridgetownrb.com/
[+]: https://shopify.github.io/liquid/
[+]: https://tailwindcss.com/
[+]: https://www.nordtheme.com/
[+]: http://github.rosettatype.com/yrsa-rasa/
[+]: https://feathericons.com/
[+]: https://docs.gitlab.com/ee/user/project/pages/
[+]: https://gitlab.com/strigocc/plume

---

Tous les textes publiés sur <%= site.metadata.host %> sont sous licence
[CC BY-SA][+].

[+]: https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr
[+]: https://creativecommons.org/licenses/by-sa/4.0/deed.fr
