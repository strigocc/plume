/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,liquid,md}"],
  theme: {
    colors: {
      current:       'currentColor',
      polar_night_1: '#2e3440',
      polar_night_2: '#3b4252',
      polar_night_3: '#434c5e',
      polar_night_4: '4c566a',
      snow_storm_1:  '#d8dee9',
      snow_storm_2:  '#e5e9f0',
      snow_storm_3:  '#eceff4',
      water:         '#8fbcbb',
      ice:           '#88c0d0',
      frozen:        '#97c0e2',
      ocean:         '#5e81ac',
      red:           '#bf616a',
      orange:        '#d08770',
      yellow:        '#ebcb8b',
      green:         '#a3be8c',
      purple:        '#bf8bb4',
    },
    fontFamily: {
      serif: ['Yrsa', 'DejaVu Serif', 'serif'],
    },
    extend: {},
  },
  plugins: [],
}
