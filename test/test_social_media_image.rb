require "uri"
require_relative "./helper"

describe "SocialMediaImageTest" do
  let(:posts) { site.collections.posts.resources }

  it "must declare an existing image file for each post" do
    posts.each do |page|
      document_root page

      img = document_root_element.at_css('head meta[property="og:image"]')
      refute_nil img, "post #{page.relative_url} image missing in YAML frontmatter"

      img_uri = URI(img.attr("content"))
      img_path = File.expand_path("../src/#{img_uri.path}", __dir__)
      assert File.exist?(img_path), "post #{page.relative_url} og:image file not found : #{img_uri.path}"
    end
  end
end
