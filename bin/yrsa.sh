#!/bin/sh

export YRSA_DIR="${YRSA_DIR:-"${HOME}/git/yrsa-rasa/fonts/Yrsa"}"
export STRIGO_PLUME_YRSA_DIR="${STRIGO_PLUME_YRSA_DIR:-"$(realpath "${0%/*}/../frontend/fonts/yrsa")"}"

version="0.1.0"

help="yrsa-web $version

Convert & compress Yrsa fonts from TTF to WOFF2
and put them into ${STRIGO_PLUME_YRSA_DIR}

Usage: ${0:t} [-h | --help] [-V | --version]

Environment:
  YRSA_DIR                Yrsa Custom fonts directory
  STRIGO_PLUME_YRSA_DIR   Strigo Plume's Yrsa fonts directory

Options:
  -h, --help                  print this help
  -V, --version               print the version number"

# options
while [[ "$1" =~ ^- && ! "$1" == "--" ]]; do case "$1" in
  -V | --version )
    echo "$version"
    exit
    ;;
  -h | --help )
    echo "$help"
    exit
    ;;
esac; shift; done

find "${YRSA_DIR}" -name "*.ttf" -execdir sh -c 'echo "${0/%ttf/woff2}"' {} \; -execdir sh -c 'pyftsubset "${0}" --flavor=woff2 --unicodes-file="${STRIGO_PLUME_YRSA_DIR}/yrsa.utf8" --output-file="${STRIGO_PLUME_YRSA_DIR}/woff2/${0/%ttf/woff2}"' {} \;
